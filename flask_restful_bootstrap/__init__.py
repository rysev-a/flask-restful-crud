from .detail_resource import DetailResource
from .list_resource import ListResource

__version__ = '0.0.4'
