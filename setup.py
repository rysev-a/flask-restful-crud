from setuptools import setup

setup(
    name="flask-restful-bootstrap",
    version="0.0.4",
    author="Rysev Alexei",
    author_email="alexeirysev@gmail.com",
    description="Bootstrap classes for flask-restful",
    long_description='Helpers for quick start rest api',
    url="https://bitbucket.org/rysev-a/flask-restful-bootstrap",
    packages=['flask_restful_bootstrap'],
    install_requires=[
        'Flask==1.0.2',
        'Flask-RESTful==0.3.6',
        'SQLAlchemy==1.2.9',
        'Flask-SQLAlchemy==2.3.2'
    ],
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
